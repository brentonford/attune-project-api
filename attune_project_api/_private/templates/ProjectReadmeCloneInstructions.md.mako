<%page args=""/>
<%
    readthedocs = "https://servertribe-attune.readthedocs.io/en/latest/"
    howtoDesign = "howto/design_workspace/"
    cloneInstructions = f"{readthedocs}{howtoDesign}clone_project.html"
%>
# Clone this Project

To clone this project into your own instance of Attune, follow the
[Clone a GIT Project How To Instructions](${cloneInstructions}).