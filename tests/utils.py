import pygit2


def countCommitsOnBranch(repo: pygit2.Repository, branch: pygit2.Branch) -> int:
    count = 0
    for _ in repo.walk(branch.target, pygit2.GIT_SORT_TOPOLOGICAL):
        count += 1
    return count


def getLatestCommitIdOnBranch(repo: pygit2.Repository, name: str) -> str:
    assert name in repo.branches
    return repo.revparse_single(name).id
