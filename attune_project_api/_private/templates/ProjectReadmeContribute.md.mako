<%page args=""/>
<%
    mediaDir = "https://www.servertribe.com/wp-content/uploads/"
    imgAttunePullRequest01 = f"{mediaDir}2023/02/Attune-pull-request-01.png"
    imgAttuneHelp02 = f"{mediaDir}2023/02/Attune-get-help-02.png"
%>
# Contribute to this Project

If you’d like to contribute and help improve these projects, please fork our
repository, commit your changes in Attune, push you changes, and create a
pull request.

Please feel free to raise any issues or questions you have.

**The collective power of a community of talented individuals working in
concert delivers not only more ideas, but quicker development and
troubleshooting when issues arise.**

# Discuss this Project in Discord

Join our [Discord channel](http://discord.servertribe.com)
and connect with like-minded individuals who share your passion. Engage in
lively discussions, gain valuable insights, and stay updated on the latest
trends in our industry. Don't miss out on this opportunity to network, learn,
and grow together.