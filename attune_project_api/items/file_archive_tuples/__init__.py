def loadStorageFileArchiveTuples():
    from . import file_archive_tuple
    from . import file_archive_large_tuple
    from . import file_archive_versioned_tuple
