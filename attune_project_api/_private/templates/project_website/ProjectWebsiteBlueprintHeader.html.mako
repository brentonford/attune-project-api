<%page args="blueprint"/>
<div class="container px-0">
    <!-- Blueprint name and description -->
    <div class="row">
        <div class="col px-0">
            <h1 class="text-left">
                How to ${blueprint.name}
            </h1>
        </div>
    </div>

<!-- Blueprint Description -->
% if blueprint.comment:
    <div class="row">
        <div class="description col px-0">
            <p class="text-left">
                ${blueprint.makeCommentHtml(4)}
            </p>
        </div>
    </div>
% endif
    <div class="row pt-4">
        <div class="col-6 px-0">
            <h6 class="text-left">
                This documentation is generated by
                <a href="https://www.servertribe.com/"
                    class="badge badge-primary"
                    target="_blank">
                    Attune
                </a>
            </h6>
        </div>
        <div class="col-6 align-self-end text-right px-0">
            <h6 class="text-right">
                <a href="https://www.youtube.com/@servertribe"
                    class="badge badge-secondary"
                    target="_blank">
                    YouTube
                </a>
                <a href="https://github.com/Attune-Automation"
                    class="badge badge-secondary"
                    target="_blank">
                    GitHub
                </a>
                <a href="https://discord.servertribe.com/"
                    class="badge badge-secondary"
                    target="_blank">
                    Discord
                </a>
                <a href="https://www.linkedin.com/company/servertribe/"
                    class="badge badge-secondary"
                    target="_blank">
                    LinkedIn
                </a>
                <a href="https://docs.attuneautomation.com"
                    class="badge badge-secondary"
                    target="_blank">
                    Documentation
                </a>
            </h6>
        </div>
    </div>
    <hr>
</div>