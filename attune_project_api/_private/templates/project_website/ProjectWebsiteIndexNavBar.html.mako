<%page args="projectMetadata"/>
<div class="bg-white border-bottom box-shadow sticky-top">
<div class="container px-0">
    <div class="row px-0">
        <div class="col d-flex flex-column flex-lg-row align-items-center px-0
            py-3">
            <a class="navbar-brand my-0 mr-lg-auto"
                  href="https://www.servertribe.com/"
                  target="_blank">
                    <img
                        src="https://www.servertribe.com/wp-content/uploads/2022/06/AttuneLogoV2_powered_by.png"
                        alt="Attune - Powered by ServerTribe"
                        class="img-fluid navbar-img">
            </a>
            <nav class="my-2 my-lg-0 mr-lg-3">
                <a class="p-2 text-dark" href="https://docs.attuneautomation.com//"
                   target="_blank">
                    Documentation
                </a>
                <a class="p-2 text-dark"
                   href="https://discord.servertribe.com/"
                   target="_blank">
                    Discord
                </a>
                <a class="p-2 text-dark" href="https://github.com/Attune-Automation"
                   target="_blank">
                    GitHub
                </a>
                <a class="p-2 text-dark" href="https://www.servertribe.com/learn/"
                   target="_blank">
                    Learn
                </a>
            </nav>
            <a class="btn btn-outline-primary"
               href="https://www.servertribe.com/download-attune-registration/"
               target="_blank">
                Download NOW!
            </a>
        </div>
    </div>
</div></div>