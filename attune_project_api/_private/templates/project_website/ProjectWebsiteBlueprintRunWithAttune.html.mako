<%page args="blueprint"/>
        <div class="jumbotron">
            <p class="display-3 text-center">
                <strong>Automate This</strong>
            </p>
            <p class="display-4 text-center">
                <strong>Get Attune</strong>
            </p>
            <p align="center">
                <a href="https://www.servertribe.com/download-attune-registration/">
                    <img src="https://img.shields.io/badge/download-latest-brightgreen.svg" /></a>
            </p>
            <hr class="my-4 text-center">
            <div class="col px-0">
                <p class="text-center">
                    Get the most out of automation with our get started
                    videos, product demonstrations, and more.
                </p>
                <p class="lead text-center">
                    <a class="btn btn-primary btn-lg"
                       href="https://www.servertribe.com/learn/"
                       target="_blank"
                       role="button">
                        Learn Attune Automation
                    </a>
                </p>
            </div>
        </div>
        <div class="row">
            <p>The following steps will guide you through the manual process.</p>
        </div>