<%page />
        <div class="row py-5">
            <div class="jumbotron">
              <h1 class="display-4 text-center">
                  Discuss this Project in Discord
              </h1>
              <p class="lead">

                  Join our Discord channel and connect with like-minded individuals who
                  share your passion. Engage in lively discussions, gain valuable insights,
                  and stay updated on the latest trends in our industry. Don't miss out on
                  this opportunity to network, learn, and grow together.
              </p>
              <hr class="my-4 text-center">
                <div class="col px-0">
                  <p class="text-center">
                      Click the link below and become a part of our vibrant community on
                      Discord today!
                  </p>
                  <p class="lead text-center">
                    <a class="btn btn-primary btn-lg"
                       href="https://discord.servertribe.com/"
                       role="button">
                        Join NOW!!!
                    </a>
                  </p>
                </div>
            </div>
        </div>