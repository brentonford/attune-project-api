<%page args=""/>
<%
    mediaDir = "https://www.servertribe.com/wp-content/uploads/"
    imgAttuneAutomation = f"{mediaDir}2022/08/attune_computer.png"
%>
<%text>
# Attune Automation

Automate and orchestrate your scripts, commands, and processes exactly as they
would be manually typed into your devices.

[Attune Automation](https://www.servertribe.com/)
is the fast, light, and easy way for System Admins to orchestrate scripts.
Easy-to-understand steps using common scripting languages that System
Administrators are already familiar with. It is an agentless solution that
connects to your devices with WinRM and ssh protocols.

</%text>
<p align="center">
  <a href="https://www.servertribe.com/" target="_blank">
    <img src="${imgAttuneAutomation}" alt="Attune Automation | Powered by ServerTribe" width="75%"/>
  </a>
</p>
<%text>

Attune's game-changing ability to fix scripts and resume automation jobs from
where they stopped empowers you to create automation significantly faster
than any other product. Before Attune, an error in an automation job meant
restarting your whole automation job from the beginning.

You can run scripts targeting multiple devices, as multiple users, within the
one Job, enabling you to perform environment wide orchestration of your scripts.

The Attune platform is building a community of sharable automated and
orchestrated processes.

You can leverage the publicly available orchestrated blueprints to increase
your productivity, and accelerate the delivery of your projects. You can
open-source your own work and improve existing community orchestrated projects.

Paste in your existing scripts and get started
[NOW!](https://www.servertribe.com/download-attune-registration/)

## Get Started with Attune, Download NOW!

The **Attune Community Edition** can be
[downloaded](https://www.servertribe.com/download-attune-registration/)
for free from our
[ServerTribe website](https://www.servertribe.com/download-attune-registration/).

You can learn more about Attune through
[ServerTribe's YouTube Channel](https://www.youtube.com/@servertribe).
</%text>