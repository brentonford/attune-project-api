<%page />
            <div class="toc col-lg-2 px-0">
                <div class="container px-0 pt-3 pl-5">
                    <div class="card border-light mb-3" style="max-width: 18rem;">
                        <div class="card-body attune-ad">
                            <img
                                src="https://www.servertribe.com/wp-content/uploads/2022/06/AttuneLogoV2_powered_by.png"
                                alt="Attune - Powered by ServerTribe"
                                class="img-fluid">
                            <p class="card-title mt-3 text-center">
                                <strong>Automate with Attune</strong>
                            </p>
                            <p class="card-text text-center">
                                Download the Attune Community Edition.
                            </p>
                            <p class="text-center">
                                <a href="https://www.servertribe.com/download-attune-registration/"
                                   class="btn btn-primary mt-3">
                                    DOWNLOAD!!!
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>