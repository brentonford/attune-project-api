<%page args="projectMetadata"/>
<footer class="footer fixed-bottom bg-white border-top box-shadow">
      <div class="container px-0">
          <div class="row">
              <div class="col px-0">
                  <a class="my-0 mr-lg-auto"
                     href="https://www.servertribe.com/"
                     target="_blank">
                      <img
                          src="https://www.servertribe.com/wp-content/uploads/2022/06/AttuneLogoV2_powered_by.png"
                          alt="Attune - Powered by ServerTribe"
                          class="img-fluid navbar-img">
                  </a>
              </div>
          </div>
      </div>
</footer>